export const sygnet = [, `
  <title>coreui logo</title>
<g>
	<g transform="translate(14.182)">
		<defs>
			<filter id="Adobe_OpacityMaskFilter" filterUnits="userSpaceOnUse" x="-4.946" y="-5.817" width="15.633" height="31.647">
				<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-4.946" y="-5.817" width="15.633" height="31.647" id="b">
			<g filter="url(#Adobe_OpacityMaskFilter)">
				<path id="a" fill="#FFFFFF" d="M2.884,0.092l-1.92,19.829c1.268-0.14,2.54-0.247,3.813-0.325L2.884,0.092L2.884,0.092z"/>
			</g>
		</mask>
		<path mask="url(#b)" fill="#FFD400" d="M-4.946,25.83h15.633V-5.817H-4.946V25.83z"/>
	</g>
	<g transform="translate(16.545)">
		<defs>
			<filter id="Adobe_OpacityMaskFilter_1_" filterUnits="userSpaceOnUse" x="-5.38" y="-5.796" width="18.33" height="31.278">
				<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-5.38" y="-5.796" width="18.33" height="31.278" id="d">
			<g filter="url(#Adobe_OpacityMaskFilter_1_)">
				<path id="c" fill="#FFFFFF" d="M0.528,0.116l1.89,19.456c1.538-0.09,3.077-0.137,4.617-0.141L7.041,0.116L0.528,0.113V0.116z"/>
			</g>
		</mask>
		<path mask="url(#d)" fill="#66B24C" d="M-5.38,25.482h18.33V-5.796H-5.38V25.482z"/>
	</g>
	<defs>
		<filter id="Adobe_OpacityMaskFilter_2_" filterUnits="userSpaceOnUse" x="-5.91" y="-5.819" width="28.885" height="35.364">
			<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
		</filter>
	</defs>
	<mask maskUnits="userSpaceOnUse" x="-5.91" y="-5.819" width="28.885" height="35.364" id="f">
		<g filter="url(#Adobe_OpacityMaskFilter_2_)">
			<path id="e" fill="#FFFFFF" d="M0,0.092l0.01,23.545c4.008-1.754,9.228-3.061,15.135-3.716l1.92-19.829V0.09L0,0.092z"/>
		</g>
	</mask>
	<path mask="url(#f)" fill="#004FB6" d="M-5.91,29.545h28.885V-5.819H-5.91V29.545z"/>
	<g transform="translate(75.636)" display="none">
		<defs>
			<filter id="Adobe_OpacityMaskFilter_3_" filterUnits="userSpaceOnUse" x="-5.907" y="-5.686" width="18.793" height="31.04">
				<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-5.907" y="-5.686" width="18.793" height="31.04" id="h" display="inline">
			<g filter="url(#Adobe_OpacityMaskFilter_3_)">
				<path id="g" fill="#FFFFFF" d="M0.003,0.223V0.83c0.418,0.075,1.912,0.511,1.912,1.575v17.041h5.061V0.222L0.003,0.223      L0.003,0.223z"/>
			</g>
		</mask>
		<path display="inline" mask="url(#h)" fill="#004FB6" d="M-5.907,25.354h18.793v-31.04H-5.907V25.354z"/>
	</g>
	<g transform="translate(29.545)" display="none">
		<defs>
			<filter id="Adobe_OpacityMaskFilter_4_" filterUnits="userSpaceOnUse" x="-5.763" y="-5.686" width="33.843" height="31.04">
				<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-5.763" y="-5.686" width="33.843" height="31.04" id="j" display="inline">
			<g filter="url(#Adobe_OpacityMaskFilter_4_)">
				<path id="i" fill="#FFFFFF" d="M6.292,0.223V0.83c0.612,0.128,1.942,0.5,1.403,1.7L0.148,19.443h3.987L6.5,14.171h8.361      l2.067,5.272h5.244L14.68,0.224L6.292,0.223L6.292,0.223z M10.96,4.223l2.964,7.56H7.57L10.96,4.223z"/>
			</g>
		</mask>
		<path display="inline" mask="url(#j)" fill="#004FB6" d="M-5.763,25.354H28.08v-31.04H-5.763V25.354z"/>
	</g>
	<g transform="translate(106.364)" display="none">
		<defs>
			<filter id="Adobe_OpacityMaskFilter_5_" filterUnits="userSpaceOnUse" x="-4.973" y="-5.686" width="33.842" height="31.04">
				<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-4.973" y="-5.686" width="33.842" height="31.04" id="l" display="inline">
			<g filter="url(#Adobe_OpacityMaskFilter_5_)">
				<path id="k" fill="#FFFFFF" d="M7.079,0.222V0.83c0.611,0.128,1.943,0.5,1.402,1.7L0.936,19.443h3.987l2.359-5.273h8.362      l2.068,5.273h5.249L15.465,0.222H7.079z M11.747,4.222l2.964,7.562H8.357L11.747,4.222L11.747,4.222z"/>
			</g>
		</mask>
		<path display="inline" mask="url(#l)" fill="#004FB6" d="M-4.973,25.354h33.842v-31.04H-4.973V25.354z"/>
	</g>
	<g transform="translate(85.09)" display="none">
		<defs>
			<filter id="Adobe_OpacityMaskFilter_6_" filterUnits="userSpaceOnUse" x="-5.373" y="-5.686" width="34.174" height="31.04">
				<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-5.373" y="-5.686" width="34.174" height="31.04" id="n" display="inline">
			<g filter="url(#Adobe_OpacityMaskFilter_6_)">
				<path id="m" fill="#FFFFFF" d="M19.105,0.223l-5.869,14.095L7.65,0.223H0.538V0.83c0.821,0.158,1.743,0.511,2.164,1.575      l6.751,17.041h5.434L22.89,0.222L19.105,0.223L19.105,0.223z"/>
			</g>
		</mask>
		<path display="inline" mask="url(#n)" fill="#004FB6" d="M-5.373,25.354h34.174v-31.04H-5.373V25.354z"/>
	</g>
	<g transform="translate(49.636)" display="none">
		<defs>
			<filter id="Adobe_OpacityMaskFilter_7_" filterUnits="userSpaceOnUse" x="-5.003" y="-5.686" width="34.174" height="31.04">
				<feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0"/>
			</filter>
		</defs>
		<mask maskUnits="userSpaceOnUse" x="-5.003" y="-5.686" width="34.174" height="31.04" id="p" display="inline">
			<g filter="url(#Adobe_OpacityMaskFilter_7_)">
				<path id="o" fill="#FFFFFF" d="M19.473,0.223l-5.868,14.095L8.02,0.223H0.906V0.83C1.729,0.988,2.65,1.341,3.072,2.405      l6.749,17.041h5.435l8.006-19.223L19.473,0.223L19.473,0.223z"/>
			</g>
		</mask>
		<path display="inline" mask="url(#p)" fill="#004FB6" d="M-5.003,25.354h34.174v-31.04H-5.003V25.354z"/>
	</g>
</g>
`]

