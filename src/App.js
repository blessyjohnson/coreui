import React from 'react';
import logo from './logo.svg';
import './scss/style.scss';
import './scss/_custom.scss';
import TheLayout from './containers/TheLayout';
import {Route, HashRouter} from 'react-router-dom';

function App() {
  return (
    <div className="App">
     <HashRouter>
      <Route path="/" name="Home" render={props => <TheLayout {...props}/>} />
     </HashRouter>
    </div>
  );
}

export default App;
